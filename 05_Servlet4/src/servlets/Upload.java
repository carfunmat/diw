package servlets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * Servlet implementation class Upload
 */
@WebServlet(name = "Upload", urlPatterns = "/upload")

@MultipartConfig(
		fileSizeThreshold = 1048576, // 1mb
		maxFileSize = 1048576, // 1mb
		maxRequestSize = 5242880) // 5mb

public class Upload extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	 private static final String UPLOAD_DIR = "uploads"; //ruta final
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		Path upload_dir = Paths.get(UPLOAD_DIR);//Crear una ruta
		 
		//Ver el path absoluto de aplicaci�n web
		String applicationPath = request.getServletContext().getRealPath("");
		System.out.println("El Path es: " + applicationPath);
		
		Path ruta = Paths.get(applicationPath);
	    Path uploadFilePath2 = Paths.get(request.getServletContext().getRealPath(""), UPLOAD_DIR);
	    System.out.println("Ruta " + uploadFilePath2);
	    
	    //Comprobar si existe la ruta y dependiendo del resultado la creamos o no
	    
	    if(Files.notExists(uploadFilePath2)) {
	    	Files.createDirectory(uploadFilePath2);
	    }
	    
	    
		//Construir Path de directorio para guardar lo que subimos
		String uploadFilePath = applicationPath; //Esto puede variar cuando estemos en explotaci�n
		String name = request.getParameter("name");
		System.out.println("name: " + name);
		String email = request.getParameter("email");
		System.out.println("email: " + email);
		
		Part archivo = request.getPart("photo");
		
		String nombre = archivo.getSubmittedFileName();
		Path nombre2 = Paths.get(archivo.getSubmittedFileName());
		
		//Escribimos el archivo al disco duro del servidor
		//archivo.write(uploadFilePath + File.separator + nombre);
		archivo.write(uploadFilePath2 + File.separator + nombre2);
		request.setAttribute("message", "Fichero subido con �xito (" + uploadFilePath2 + ")");
		getServletContext().getRequestDispatcher("/response.jsp").forward(request, response);
	}

}
