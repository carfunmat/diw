package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HolaMundo extends HttpServlet{
	
	//Clase creada desde el ide como New Class
	//Importande que herede de HttpServlet
	//Ver mapeo en fichero web.xml
	/*
	 *  <servlet>
    	<servlet-name>HolaMundo</servlet-name> 				Este nombre de servlet
    	<servlet-class>servlets.HolaMundo</servlet-class>	Corresponde a esta clase de Java
  		</servlet>
	 */
	/*
	 *  <servlet-mapping>
    	<servlet-name>HolaMundo</servlet-name>				Expone la clase al navegador. El nombre tiene que ser igual que arriba
    	<url-pattern>/holamundo</url-pattern>				La url declarada no tiene por que coincidir
  		</servlet-mapping>
	 */

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//Atiende peticiones llegadas desde un link por url
		System.out.println("M�todo doGet del Servlet HolaMundo incluido en el package servlets");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//Atiende peticiones llegadas desde un formulario mediante el acction del formulario
		System.out.println("M�todo doPost del Servlet HolaMundo incluido en el package servlets");		
	}
}
