package beans;

import java.util.LinkedList;
import java.util.List;

public class ProductoDB {
	 private static List<Producto> catalogo;

	    public ProductoDB() {
	        catalogo = new LinkedList<>(); 
	        // Instanciamos 5 objetos "Producto" que podr�an ser recuperados desde una base de datos.
	        Producto p1= new Producto(1);
	        p1.setCategoria("Informatica");
	        p1.setNombre("Portatil Asus");
	        p1.setPrecio(400);
	        
	        Producto p2= new Producto(2);
	        p2.setCategoria("Imagen y Sonido");
	        p2.setNombre("Televisor Sony");
	        p2.setPrecio(600);
	        
	        Producto p3= new Producto(3);
	        p3.setCategoria("Telefonia movil");
	        p3.setNombre("IPhone 5s");
	        p3.setPrecio(350);
	        
	        Producto p4= new Producto(4);
	        p4.setCategoria("Telefonia movil");
	        p4.setNombre("LG G4");
	        p4.setPrecio(180);
	        
	        Producto p5= new Producto(5);
	        p5.setCategoria("Telefonia movil");
	        p5.setNombre("Samsung A20");
	        p5.setPrecio(195);
	        
	        Producto p6= new Producto(6);
	        p6.setCategoria("Imagen y Sonido");
	        p6.setNombre("Samsung Super Grande");
	        p6.setPrecio(795);
	        
	        Producto p7= new Producto(7);
	        p7.setCategoria("Telefonia movil");
	        p7.setNombre("Samsung A40");
	        p7.setPrecio(320);
	        
	        Producto p8= new Producto(8);
	        p8.setCategoria("Telefonia movil");
	        p8.setNombre("Samsung A40");
	        p8.setPrecio(320);
	        
	        Producto p9= new Producto(9);
	        p9.setCategoria("Telefonia movil");
	        p9.setNombre("Samsung A40");
	        p9.setPrecio(320);
	        
	        Producto p10= new Producto(10);
	        p10.setCategoria("Telefonia movil");
	        p10.setNombre("Samsung A40");
	        p10.setPrecio(320);
	        
	        Producto p11= new Producto(11);
	        p11.setCategoria("Telefonia movil");
	        p11.setNombre("Samsung A40");
	        p11.setPrecio(320);
	        
	        //Agregamos los objetos a una lista para recorerla desde la tabla
	        catalogo.add(p1);
	        catalogo.add(p2);
	        catalogo.add(p3);
	        catalogo.add(p4);
	        catalogo.add(p5);
	        catalogo.add(p6);
	        catalogo.add(p7);
	        catalogo.add(p8);
	        catalogo.add(p9);
	        catalogo.add(p10);
	        catalogo.add(p11);
	        
	    }
	    
	    public List<Producto> getAll(){
	        return catalogo;
	    }	

}
