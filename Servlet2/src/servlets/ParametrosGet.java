package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ParametrosGet
 */
@WebServlet("/ParametrosGet")
public class ParametrosGet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Recibimos parámetros y los asignamos a variables Java
				String id=request.getParameter("id");
				String nombre=request.getParameter("nombre");
				
				//Imprimimos los parámetros recibidos
				System.out.println("Método doGet que recibe parámetros: Id ("+ id +") y nombre ("+ nombre +")");
				
				//Aplicamos la lógica de nuestro modelo de negocio
				//Si el id del usuario es mayor de 5, se devolverá una respuesta HTML"
		        if (id.equals("10")) {
		            response.setContentType("text/html;charset=UTF-8");
		            
		            try (PrintWriter out2 = response.getWriter();) {
		            	out2.println("<!DOCTYPE html>");
		            	out2.println("<html>");
		            	out2.println("<head>");
		                out2.println("<title>Login</title>");
		                out2.println("</head>");
		                out2.println("<body>");
		                out2.println("<h3>Identificador de usuario ("+id+" correcto)</h3>");
		                out2.println("</body>");
		                out2.println("</html>");
		            }
		        } else {
		        	//Respuesta de error indicando que no estamos autorizados
		            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Metodo Post");
	}

}
