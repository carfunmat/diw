package servlets;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Imagen
 */
@WebServlet(description = "Descargar imagen con servlet", urlPatterns = { "/Imagen" })
public class Imagen extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	  response.setContentType("image/jpeg");
	
//      String pathToWeb = getServletContext().getRealPath(File.separator);
//      System.out.println("pathToWeb = " + pathToWeb);
//      File f = new File(pathToWeb + "/imagenes/linux_distros.jpg");
//      BufferedImage bi = ImageIO.read(f);
//      OutputStream out = response.getOutputStream();
//      ImageIO.write(bi, "jpg", out);
//      out.close();
      
      Path pathToWeb2 = Paths.get(getServletContext().getRealPath(File.separator));
      System.out.println("pathToWeb2 = " + pathToWeb2);
      File f2 = new File(pathToWeb2 + "/imagenes/ReyesMagos.jpg");
      BufferedImage bi2 = ImageIO.read(f2);
      OutputStream out2 = response.getOutputStream();
      ImageIO.write(bi2, "jpg", out2);
      out2.close();
      
	}

}
